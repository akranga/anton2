apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: ${application.name}-${application.source.bucket.name}-bucketevents-
spec:
  entrypoint: main
  volumes:
  - name: dockersock
    hostPath:
      path: /var/run/docker.sock
  arguments:
    parameters:
    - name: file
      value: undefined
    - name: application
      value: ${application.name}
    - name: processorInputDir
      value: /workspace
    - name: processorOutputDir
      value: /workspace
    - name: image
      value: ${application.docker.image}
    - name: pullSecret
      value: ${component.pullSecret.name}
    - name: sourceBucket
      value: ${application.source.bucket.name}
    - name: sourceBucketEndpoint
      value: ${application.source.bucket.endpoint}
    - name: sourceBucketSecret
      value: ${application.source.bucket.secret.name}
    - name: sourceBucketAccessKeyRef
      value: ${application.source.bucket.secret.accessKeyRef}
    - name: sourceBucketSecretKeyRef
      value: ${application.source.bucket.secret.secretKeyRef}
    - name: targetBucket
      value: ${application.target.bucket.name}
    - name: targetBucketEndpoint
      value: ${application.target.bucket.endpoint}
    - name: targetBucketSecret
      value: ${application.target.bucket.secret.name}
    - name: targetBucketAccessKeyRef
      value: ${application.target.bucket.secret.accessKeyRef}
    - name: targetBucketSecretKeyRef
      value: ${application.target.bucket.secret.secretKeyRef}
  templates:
  - name: main
    steps:
    - - name: login
        template: awscli
        arguments:
          parameters:
          - name: script
            value: >
              aws ecr get-login --region ${cloud.region} --no-include-email | bash -
      - name: process
        template: processor
        arguments:
          parameters:
          - name: script
            value: >
              echo processing file {{workflow.parameters.file}}
    - - name: finalize
        template: cleanup-script
        arguments:
          parameters:
          - name: script
            value: >
              mc rm source/{{workflow.parameters.sourceBucket}}/{{workflow.parameters.file}}
- name: awscli
    inputs:
      parameters:
      - name: script
    script:
      image: agilestacks/toolbox
      securityContext:
        privileged: true
      volumeMounts:
      - name: dockersock
        mountPath: "/var/run/docker.sock"
      env:
      - name: AWS_REGION
        value: ${cloud.region}
      command: [bash]
      source: "{{inputs.parameters.script}}"

  - name: cleanup-script
    inputs:
      parameters:
      - name: script
    script:
      image: agilestacks/toolbox
      env:
      - name: MC_HOSTS_source
        valueFrom:
          secretKeyRef:
            name: "{{workflow.parameters.application}}-app"
            key: source.bucket.mcHost
      - name: MC_HOSTS_target
        valueFrom:
          secretKeyRef:
            name: "{{workflow.parameters.application}}-app"
            key: target.bucket.mcHost
      command: [bash]
      source: "{{inputs.parameters.script}}"

  - name: processor
    container:
      image: "{{workflow.parameters.image}}"
      command: [sh, -c]
      workingDir: "{{workflow.parameters.processorInputDir}}"
      args: ["{{inputs.parameters.script}}"]
    imagePullSecrets:
    - name: {{inputs.parameters.pullSecret}}
    inputs:
      parameters:
      - name: script
      artifacts:
      - name: bucket
        path: "{{workflow.parameters.processorInputDir}}/{{workflow.parameters.file}}"
        s3:
          endpoint: "{{workflow.parameters.sourceBucketEndpoint}}"
          insecure: true
          bucket: "{{workflow.parameters.sourceBucket}}"
          key: "{{workflow.parameters.file}}"
          accessKeySecret:
            name: "{{workflow.parameters.sourceBucketSecret}}"
            key: "{{workflow.parameters.sourceBucketAccessKeyRef}}"
          secretKeySecret:
            name: "{{workflow.parameters.sourceBucketSecret}}"
            key: "{{workflow.parameters.sourceBucketSecretKeyRef}}"
    outputs:
      artifacts:
      - name: bucket
        path: "{{workflow.parameters.processorOutputDir}}"
        archive:
          none: {}
        s3:
          endpoint: "{{workflow.parameters.targetBucketEndpoint}}"
          insecure: true
          bucket: "{{workflow.parameters.targetBucket}}"
          key: ""
          accessKeySecret:
            name: "{{workflow.parameters.targetBucketSecret}}"
            key: "{{workflow.parameters.targetBucketAccessKeyRef}}"
          secretKeySecret:
            name: "{{workflow.parameters.targetBucketSecret}}"
            key: "{{workflow.parameters.targetBucketSecretKeyRef}}"
