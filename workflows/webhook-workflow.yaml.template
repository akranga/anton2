apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: ${application.name}-webhook-
spec:
  entrypoint: workflow
  volumes:
  - name: dockersock
    hostPath:
      path: /var/run/docker.sock
  arguments:
    parameters:
    - name: application
      value: ${application.name}
    - name: image
      value: ${application.docker.image}
    - name: pullSecret
      value: ${component.pullSecret.name}
    - name: processorInputDir
      value: /workspace
    - name: processorOutputDir
      value: /workspace
    - name: sourceBucket
      value: ${application.source.bucket.name}
    - name: sourceBucketEndpoint
      value: ${application.source.bucket.endpoint}
    - name: sourceBucketSecret
      value: ${application.source.bucket.secret.name}
    - name: sourceBucketAccessKeyRef
      value: ${application.source.bucket.secret.accessKeyRef}
    - name: sourceBucketSecretKeyRef
      value: ${application.source.bucket.secret.secretKeyRef}
    - name: targetBucket
      value: ${application.target.bucket.name}
    - name: targetBucketEndpoint
      value: ${application.target.bucket.endpoint}
    - name: targetBucketSecret
      value: ${application.target.bucket.secret.name}
    - name: targetBucketAccessKeyRef
      value: ${application.target.bucket.secret.accessKeyRef}
    - name: targetBucketSecretKeyRef
      value: ${application.target.bucket.secret.secretKeyRef}
  templates:
  - name: workflow
    steps:
    - - name: list-all
        template: toolbox-script
        arguments:
          parameters:
          - name: script
            value: >
              mc ls source/{{workflow.parameters.sourceBucket}} --recursive --json
              | jq -sc '. | [.[].key]'

    - - name: take-one
        template: process-steps
        withParam: "{{steps.list-all.outputs.result}}"
        arguments:
          parameters:
          - name: file
            value: "{{item}}"

  - name: process-steps
    inputs:
      parameters:
      - name: file
      - name: upstream-bucket
        value: "{{workflow.parameters.sourceBucket}}"
    steps:
    - - name: process
        template: processor
        arguments:
          parameters:
          - name: file
            value: "{{inputs.parameters.file}}"
          - name: script
            value: >
              echo processing file {{inputs.parameters.file}}
    - - name: finalize
        template: toolbox-script
        arguments:
          parameters:
          - name: script
            value: >
              mc rm source/{{workflow.parameters.sourceBucket}}/{{inputs.parameters.file}}

  - name: toolbox-script
    inputs:
      parameters:
      - name: script
    script:
      image: agilestacks/toolbox
      securityContext:
        privileged: true
      env:
      - name: MC_HOSTS_source
        valueFrom:
          secretKeyRef:
            name: "{{workflow.parameters.application}}-app"
            key: upstream.bucket.mcHost
      - name: MC_HOSTS_downstream
        valueFrom:
          secretKeyRef:
            name: "{{workflow.parameters.application}}-app"
            key: downstream.bucket.mcHost
      command: [bash]
      source: "{{inputs.parameters.script}}"

  - name: processor
    container:
      image: "{{workflow.parameters.image}}"
      command: [sh, -c]
      workingDir: "{{workflow.parameters.processorInputDir}}"
      args: ["{{inputs.parameters.script}}"]
    imagePullSecrets:
    - name: {{inputs.parameters.pullSecret}}
    inputs:
      parameters:
      - name: file
      - name: script
      artifacts:

          key: "{{workflow.parameters.file}}"

      - name: bucket
        path: "{{workflow.parameters.processorInputDir}}/{{inputs.parameters.file}}"
        s3:
          endpoint: "{{workflow.parameters.sourceBucketEndpoint}}"
          insecure: true
          bucket: "{{workflow.parameters.sourceBucket}}"
          key: "{{inputs.parameters.file}}"
          accessKeySecret:
            name: "{{workflow.parameters.sourceBucketSecret}}"
            key: "{{workflow.parameters.sourceBucketAccessKeyRef}}"
          secretKeySecret:
            name: "{{workflow.parameters.sourceBucketSecret}}"
            key: "{{workflow.parameters.sourceBucketSecretKeyRef}}"
    outputs:
      - name: bucket
        path: "{{workflow.parameters.processorOutputDir}}"
        archive:
          none: {}
        s3:
          endpoint: "{{workflow.parameters.targetBucketEndpoint}}"
          insecure: true
          bucket: "{{workflow.parameters.targetBucket}}"
          key: ""
          accessKeySecret:
            name: "{{workflow.parameters.targetBucketSecret}}"
            key: "{{workflow.parameters.targetBucketAccessKeyRef}}"
          secretKeySecret:
            name: "{{workflow.parameters.targetBucketSecret}}"
            key: "{{workflow.parameters.targetBucketSecretKeyRef}}"
