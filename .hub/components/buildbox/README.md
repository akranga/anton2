# Buildbox

It is quite often when developers wants to run CI/CD with the docker containers. 
It is also often when they cannot build their application with generic runtime 
container because there are no build tools inside. Then developer have to create 
their own customized build container with dev tools inside. We call such 
container a **buildbox** .

We know that:
* Buildbox is a container that must be built before developer creates her CI/CD pipeline.
* Ideally `Dockerfile` of the buildbox should be hosted together with the application that 
it builds. Because dev tools evolves together with the application.

This repository contains a buildbox operator. A program written in general purpose
programming lanague (Golang). This program automates following operations:

* Create/delete a Github webook hook
* Receive `"Push"` notifications coming from the SCM to build a new version of the buildbox container

# Install

We are big fans of `direnv`. We advice you to create a `.envrc` file with the content
that you might want to customize for better your forcstation setup. 

Example `.envrc` can be found below:
```bash
#!/usr/bin/env bash
export GOOS=$(uname -s | tr A-Z a-z)
export GOPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export GOBIN=${GOPATH}/bin
export PATH=${GOBIN}:${PATH}
```

To build and install run follwowing command:
```bash
source .envrc
make insall
make
```

# Usage

Buildbox operator is under active development. To stay up-to-date with capabilities, refer to:

```bash
buildbox --help
```

# Development Locally

Github have got a good article for development locally: https://developer.github.com/webhooks/configuring/

### NGrok

To install:
```bash
brew cask install ngrok
```

To run:
```bash
make ngrok
```
